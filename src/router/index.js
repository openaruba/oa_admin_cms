import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/Home.vue'
import About from '@/views/About'
// import Callback from '@/views/Callback'

import { routeGuard } from '../plugins/auth0.js'

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    beforeEnter: routeGuard
  },
  // {
  //   path: '/callback',
  //   name: 'Callback',
  //   component: Callback
  // },
  {
    path: '/profile',
    name: 'Profile',
    beforeEnter: routeGuard,
    component: () => import('../views/Profile.vue'),
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router