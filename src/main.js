import { createApp } from 'vue'

import App from './App.vue'
import router from './router'
// import store from './store'
import authConfig from '../auth_config.json'
import auth0Plugin from './plugins/auth0'

const auth0PluginOptions = {
  callbackRedirect: (appState) => {
    router.push(
      appState && appState.targetUrl
        ? appState.targetUrl
        : window.location.pathname
    );
  },
  domain: authConfig.domain,
  client_id: authConfig.client_id,
  audience: authConfig.audience,
  redirect_uri: window.location.origin
}

const app = createApp(App)
app
    .use(router)
    .use(auth0Plugin, auth0PluginOptions)
    .mount('#app')