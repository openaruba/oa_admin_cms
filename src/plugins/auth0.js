import createAuth0Client from '@auth0/auth0-spa-js'
import { computed, reactive, watchEffect } from 'vue'

let client

const state = reactive({
  loading: true,
  isAuthenticated: false,
  user: {},
  popupOpen: false,
  error: null,
})

const authPlugin = {
  getIdTokenClaims,
  getTokenSilently,
  getTokenWithPopup,
  loginWithRedirect,
  loginWithPopup,
  logout
}

async function loginWithPopup(o) {
  state.popupOpen = true

  try {
    await client.loginWithPopup(o)
  } catch (e) {
    state.error = e
  } finally {
    state.popupOpen = false
  }

  state.user = await client.getUser()
  state.isAuthenticated = true
}

function loginWithRedirect(o) {
  return client.loginWithRedirect(o)
}

function getIdTokenClaims(o) {
  return client.getIdTokenClaims(o)
}

function getTokenSilently(o) {
  return client.getTokenSilently(o)
}

function getTokenWithPopup(o) {
  return client.getTokenWithPopup(o)
}

function logout(o) {
  return client.logout(o)
}

async function setupAuth (auth0PluginOptions) {
  
  client = await createAuth0Client({
    domain: auth0PluginOptions.domain,
    client_id: auth0PluginOptions.client_id,
    audience: auth0PluginOptions.audience,
    redirect_uri: auth0PluginOptions.redirect_uri
  })

  try {
    // If the user is returning to the app after authentication
    if (
      window.location.search.includes('code=') &&
      window.location.search.includes('state=')
    ) {
      // handle the redirect and retrieve tokens
      const { appState } = await client.handleRedirectCallback()

      // Notify subscribers that the redirect callback has happened, passing the appState
      // (useful for retrieving any pre-authentication state)
      auth0PluginOptions.callbackRedirect(appState)
    }
  } catch (e) {
    state.error = e
  } finally {
    // Initialize our internal authentication state
    state.isAuthenticated = await client.isAuthenticated()
    state.user = await client.getUser()
    console.log("user: ", state.user)
    state.loading = false
  }
}

export const routeGuard = (to, from, next) => {
  const { isAuthenticated, loading, loginWithRedirect } = authPlugin

  const verify = () => {
    // If the user is authenticated, continue with the route
    if (isAuthenticated.value) {
      return next()
    }

    // Otherwise, log in
    loginWithRedirect({ appState: { targetUrl: to.fullPath } })
  }

  // If loading has already finished, check our auth state using verify()
  if (!loading.value) {
    return verify()
  }

  // Watch for the loading property to change before we check isAuthenticated
  watchEffect(() => {
    if (loading.value === false) {
      return verify()
    }
  })
}

export default {
  install: (app, options) => {
    authPlugin.isAuthenticated = computed(() => state.isAuthenticated)
    authPlugin.loading = computed(() => state.loading)
    authPlugin.user = computed(() => state.user)

    app.provide('Auth', authPlugin)
    console.log("authPlugin: ", authPlugin)
    
    setupAuth(options)
  }
}